package com.kredivo.test.activity;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import com.kredivo.test.R;
import com.kredivo.test.adapter.TabPagerAdapter;
import com.kredivo.test.dummy.Dummy;
import com.kredivo.test.fragment.TopupAmountOptionFragment;
import com.kredivo.test.view.CustomViewPager;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;

public class TopupActivity extends BaseActivity {

    public static final String TAG = TopupActivity.class.getName();

    @BindView(R.id.viewpager) CustomViewPager viewPager ;
    @BindView(R.id.tablayout) TabLayout tabLayout ;


    TabPagerAdapter tabPagerAdapter ;

    @Override
    public int contentXmlLayout() {
        return R.layout.activity_topup;
    }

    @Override
    public String getTAG() {
        return TAG;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        List<String> titles = Arrays.asList(getString(R.string.phone_balance), getString(R.string.data_package));
        tabPagerAdapter = new TabPagerAdapter(
                getSupportFragmentManager(),
                titles,
                Arrays.asList(
                        TopupAmountOptionFragment.instance(Dummy.PHONE_BALANCE),
                        TopupAmountOptionFragment.instance(Dummy.QUOTA_BALANCE))
        );
        viewPager.setAdapter(tabPagerAdapter);
        tabLayout.setupWithViewPager(viewPager, true);

    }
}
