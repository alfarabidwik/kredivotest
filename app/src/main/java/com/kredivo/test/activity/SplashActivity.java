package com.kredivo.test.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;

import com.kredivo.test.R;


public class SplashActivity extends BaseActivity {

    public static final String TAG = SplashActivity.class.getName();

    @Override
    public int contentXmlLayout() {
        return R.layout.activity_splash;
    }

    @Override
    public String getTAG() {
        return TAG;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        windowView().postDelayed(() -> {
            startActivity(new Intent(SplashActivity.this, TopupActivity.class));
            finish();
        }, 100);
    }

}
