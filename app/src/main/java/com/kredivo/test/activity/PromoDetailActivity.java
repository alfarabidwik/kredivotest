package com.kredivo.test.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.alfarabi.alfalibs.tools.GlideApp;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.hendraanggrian.bundler.annotations.BindExtra;
import com.kredivo.test.R;
import com.kredivo.test.model.Promo;
import com.kredivo.test.utils.DateUtils;
import com.kredivo.test.utils.AppStringUtils;

import org.sufficientlysecure.htmltextview.HtmlTextView;

import butterknife.BindView;

public class PromoDetailActivity extends BaseActivity {

    public static final String TAG = PromoDetailActivity.class.getName();

    @BindExtra Promo promo ;

    @BindView(R.id.imageview) ImageView imageView ;
    @BindView(R.id.title_tv) TextView titleTextView ;
    @BindView(R.id.valid_date_tv) TextView validDateTextView ;
    @BindView(R.id.voucher_code_tv) TextView voucherCodeTextView ;
    @BindView(R.id.copy_button_layout) View copyButtonLayout ;
    @BindView(R.id.html_tv) HtmlTextView htmlTextView ;


    @Override
    public int contentXmlLayout() {
        return R.layout.activity_promo_detail;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        GlideApp.with(this).load(promo.getUrl()).diskCacheStrategy(DiskCacheStrategy.ALL).into(imageView);

        titleTextView.setText(promo.getTitle());
        validDateTextView.setText(DateUtils.validDate(promo.getStartDate(), promo.getEndDate()));
        voucherCodeTextView.setText(promo.getVoucherCode());
        htmlTextView.setHtml(promo.getTermAndCondition());
        copyButtonLayout.setOnClickListener(v -> {
            AppStringUtils.copy(this, promo.getVoucherCode());
            Toast.makeText(this, getString(R.string.successfully_copied), Toast.LENGTH_SHORT).show();
        });
    }
}
