package com.kredivo.test.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Html;
import android.text.Spanned;
import android.text.util.Linkify;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.alfarabi.alfalibs.tools.InputTools;
import com.hendraanggrian.bundler.Bundler;
import com.hendraanggrian.bundler.annotations.BindExtra;
import com.kredivo.test.R;
import com.kredivo.test.model.Provider;
import com.kredivo.test.model.TopupAmount;
import com.kredivo.test.utils.CommonView;
import com.kredivo.test.utils.Constant;
import com.kredivo.test.utils.NumberUtil;
import org.sufficientlysecure.htmltextview.HtmlTextView;

import java.math.BigDecimal;

import butterknife.BindView;
import butterknife.OnClick;


public class LoanConfirmationActivity extends BaseActivity {
    
    public static final String TAG = LoanConfirmationActivity.class.getName();

    @BindView(R.id.imageview) ImageView imageView;
    @BindView(R.id.phone_number_tv) TextView phoneNumberTextView ;
    @BindView(R.id.product_name_tv) TextView productNameTextView ;
    @BindView(R.id.product_price_tv) TextView productPriceTextView ;
    @BindView(R.id.admin_fee_tv) TextView adminFeeTextView ;
    @BindView(R.id.pay_in_total_tv) TextView payInTotalTextView ;
    @BindView(R.id.agreement_notice_tv) HtmlTextView agreementTextView ;
    @BindView(R.id.pin_et) EditText pinEditText ;


    @BindExtra TopupAmount topupAmount ;
    @BindExtra Provider provider ;
    @BindExtra String phoneNumber ;


    @Override
    public int contentXmlLayout() {
        return R.layout.activity_loan_confirmation;
    }

    @Override
    public String getTAG() {
        return TAG;
    }


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        int imageId = context.getResources().getIdentifier(provider.getCompanyLogo(), "drawable", context.getPackageName());
        imageView.setImageResource(imageId);
        phoneNumberTextView.setText(phoneNumber);
        productNameTextView.setText(topupAmount.getName());
        productPriceTextView.setText(NumberUtil.asMoney(topupAmount.getAmount(), true));
        adminFeeTextView.setText(NumberUtil.asMoney(BigDecimal.ZERO, true));
        payInTotalTextView.setText(NumberUtil.asMoney(topupAmount.getAmount(), true));
        Spanned agrementSpanned = null ;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            agrementSpanned = Html.fromHtml(getString(R.string.by_continuing_i_agree_with_loan_agreement_of_kredivo), Html.FROM_HTML_MODE_LEGACY);
        } else {
            agrementSpanned = Html.fromHtml(getString(R.string.by_continuing_i_agree_with_loan_agreement_of_kredivo));
        }
        if(agrementSpanned!=null){
            agreementTextView.setText(agrementSpanned);
            CommonView.setTextViewHTML(agreementTextView, getString(R.string.by_continuing_i_agree_with_loan_agreement_of_kredivo));
        }
    }

    @OnClick(R.id.pay_button)
    void onClick(){
        if(InputTools.isComplete(false, pinEditText)){
            Intent intent = new Intent(this, PaymentDetailActivity.class);
            intent.putExtras(Bundler.wrap(PaymentDetailActivity.class, topupAmount, provider, phoneNumber));
            startActivityForResult(intent, Constant.REQUEST_TOPUP);
        }else {
            showTooltip(pinEditText, "Please enter your pin");
        }
    }
}
