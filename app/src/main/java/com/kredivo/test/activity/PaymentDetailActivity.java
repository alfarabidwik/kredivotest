package com.kredivo.test.activity;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.alfarabi.alfalibs.tools.WLog;
import com.hendraanggrian.bundler.annotations.BindExtra;
import com.kredivo.test.R;
import com.kredivo.test.model.Provider;
import com.kredivo.test.model.TopupAmount;
import com.kredivo.test.utils.AppStringUtils;
import com.kredivo.test.utils.Constant;
import com.kredivo.test.utils.NumberUtil;

import java.math.BigDecimal;

import butterknife.BindView;
import butterknife.OnClick;


public class PaymentDetailActivity extends BaseActivity {

    public static final String TAG = PaymentDetailActivity.class.getName();

    @BindView(R.id.imageview) ImageView imageView;
    @BindView(R.id.phone_number_tv) TextView phoneNumberTextView ;
    @BindView(R.id.product_name_tv) TextView productNameTextView ;
    @BindView(R.id.product_price_tv) TextView productPriceTextView ;
    @BindView(R.id.admin_fee_tv) TextView adminFeeTextView ;
    @BindView(R.id.pay_in_total_tv) TextView payInTotalTextView ;
    @BindView(R.id.order_id_tv) TextView orderIdTextView ;


    @BindExtra TopupAmount topupAmount ;
    @BindExtra Provider provider ;
    @BindExtra String phoneNumber ;




    @Override
    public int contentXmlLayout() {
        return R.layout.activity_payment_detail;
    }

    @Override
    public String getTAG() {
        return TAG;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setHomeAsUpIndicator(android.R.drawable.ic_menu_close_clear_cancel);

        int imageId = context.getResources().getIdentifier(provider.getCompanyLogo(), "drawable", context.getPackageName());
        imageView.setImageResource(imageId);

        phoneNumberTextView.setText(phoneNumber);
        productNameTextView.setText(topupAmount.getName());
        productPriceTextView.setText(NumberUtil.asMoney(topupAmount.getAmount(), true));
        adminFeeTextView.setText(NumberUtil.asMoney(BigDecimal.ZERO, true));
        payInTotalTextView.setText(NumberUtil.asMoney(topupAmount.getAmount(), true));
        orderIdTextView.setText(AppStringUtils.randomCode(10));

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()==android.R.id.home){
            setResult(Constant.RESULT_FINISH);
        }
        return super.onOptionsItemSelected(item);

    }

    @OnClick(R.id.ok_button)
    void onClick(){
        setResult(Constant.RESULT_FINISH);
        finish();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if(keyCode==KeyEvent.KEYCODE_BACK){
            setResult(Constant.RESULT_FINISH);
        }
        return super.onKeyDown(keyCode, event);
    }
}
