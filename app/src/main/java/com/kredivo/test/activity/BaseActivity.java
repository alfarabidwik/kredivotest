package com.kredivo.test.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import com.alfarabi.alfalibs.activity.APIBaseActivity;
import com.alfarabi.alfalibs.fragments.interfaze.RecyclerCallback;
import com.github.florent37.viewtooltip.ViewTooltip;
import com.hendraanggrian.bundler.Bundler;
import com.kredivo.test.R;
import com.kredivo.test.utils.Constant;
import com.kredivo.test.utils.LanguageTools;
import butterknife.BindView;
import butterknife.ButterKnife;

public abstract class BaseActivity extends APIBaseActivity implements RecyclerCallback {

    public static final String TAG = BaseActivity.class.getName();


    @Nullable@BindView(R.id.toolbar) protected Toolbar toolbar ;

    public static Context context ;

    @Override
    public String getTAG() {
        return TAG;
    }


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Thread.setDefaultUncaughtExceptionHandler((thread, throwable) -> {
            throwable.printStackTrace();
        });
        LanguageTools.initLanguage(this);
        setContentView(contentXmlLayout());
        ButterKnife.bind(this);
        if(toolbar!=null){
            try{
                if(toolbar.getNavigationIcon()!=null){
                    toolbar.getNavigationIcon().setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
                }
            }catch (Exception e){
                e.printStackTrace();
            }
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }
        Bundler.bind(this);
        context = this;
    }

    public View windowView(){
        return getWindow().getDecorView();
    }

    @Override
    public Object getObject() {
        return null;
    }

    public void showTooltip(View view, String message){
        ViewTooltip.on(view).autoHide(true, 2000).color(Color.RED).textColor(Color.WHITE)
                .corner(30).position(ViewTooltip.Position.BOTTOM)
                .text(message).show();
    }

    @Override
    public void showSnackbar(String message, View.OnClickListener onClickListener) {
        final Snackbar snackbar = Snackbar.make(windowView(), message, Snackbar.LENGTH_LONG);
        snackbar.setAction("OK", v -> {
            snackbar.dismiss();
            onClickListener.onClick(v);
        });
        snackbar.show();
    }

    public void showSnackbar(Throwable throwable, View.OnClickListener onClickListener) {
        final Snackbar snackbar = Snackbar.make(windowView(), throwable.getMessage(), Snackbar.LENGTH_LONG);
        snackbar.setAction("OK", v -> {
            snackbar.dismiss();
            onClickListener.onClick(v);
        });
        snackbar.show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        context = this ;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        setResult(resultCode);
        if(resultCode==Constant.RESULT_FINISH){
            finish();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()==android.R.id.home){
            onBackPressed();
            return false ;
        }
        return super.onOptionsItemSelected(item);
    }

}
