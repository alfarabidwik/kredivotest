package com.kredivo.test.holder;

import android.content.Intent;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.alfarabi.alfalibs.adapters.recyclerview.viewholder.SimpleViewHolder;
import com.alfarabi.alfalibs.tools.Gilimanuk;
import com.alfarabi.alfalibs.tools.GlideApp;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.hendraanggrian.bundler.Bundler;
import com.kredivo.test.R;
import com.kredivo.test.activity.PromoDetailActivity;
import com.kredivo.test.fragment.TopupAmountOptionFragment;
import com.kredivo.test.model.Promo;

import butterknife.BindView;

public class BottomPromoHolder extends SimpleViewHolder<TopupAmountOptionFragment, Promo, String> {

    @BindView(R.id.imageview) ImageView imageView ;

    public BottomPromoHolder(TopupAmountOptionFragment fragment, ViewGroup viewGroup) {
        super(fragment, R.layout.holder_bottom_snap, viewGroup);
    }

    @Override
    public void showData(Promo object) {
        super.showData(object);
        GlideApp.with(getFragment().act()).load(object.getUrl()).diskCacheStrategy(DiskCacheStrategy.ALL).into(imageView);
        itemView.setOnClickListener(v -> {
            Intent intent = new Intent(getFragment().act(), PromoDetailActivity.class);
            intent.putExtras(Bundler.wrap(PromoDetailActivity.class, object));
            Gilimanuk.startActivity(getFragment().act(), intent, false);
        });
    }

    @Override
    public void find(String findParam) {

    }
}
