package com.kredivo.test.holder;

import android.content.Intent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.alfarabi.alfalibs.adapters.recyclerview.viewholder.ACTViewHolder;
import com.alfarabi.alfalibs.adapters.recyclerview.viewholder.SimpleViewHolder;
import com.alfarabi.alfalibs.tools.Gilimanuk;
import com.hendraanggrian.bundler.Bundler;
import com.kredivo.test.R;
import com.kredivo.test.activity.LoanConfirmationActivity;
import com.kredivo.test.fragment.TopupAmountOptionFragment;
import com.kredivo.test.model.TopupAmount;
import com.kredivo.test.utils.NumberUtil;

import butterknife.BindView;

public class TopupAmountHolder extends SimpleViewHolder<TopupAmountOptionFragment, TopupAmount, String> {


    @BindView(R.id.amount_tv) TextView amountTextView ;
    @BindView(R.id.select_button) View selectButton ;
    @BindView(R.id.price_tv) TextView priceTextView ;

    public TopupAmountHolder(TopupAmountOptionFragment fragment, ViewGroup viewGroup) {
        super(fragment, R.layout.holder_topup_amount, viewGroup);
    }

    @Override
    public void showData(TopupAmount object) {
        super.showData(object);
        amountTextView.setText(NumberUtil.asMoney(object.getAmount(), false));
        priceTextView.setText(NumberUtil.asMoney(object.getAmount(), true));
        selectButton.setOnClickListener(v -> {
            Intent intent = new Intent(getFragment().act(), LoanConfirmationActivity.class);
            intent.putExtras(Bundler.wrap(LoanConfirmationActivity.class, object, getFragment().getProvider(), getFragment().getCellNumberEditText().getText().toString()));
            Gilimanuk.startActivity(getFragment().act(), intent, false);
        });
    }

    @Override
    public void find(String findParam) {

    }
}
