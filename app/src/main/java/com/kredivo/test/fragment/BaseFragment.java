package com.kredivo.test.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.alfarabi.alfalibs.fragments.api.APIBaseFragment;
import com.alfarabi.alfalibs.fragments.interfaze.RecyclerCallback;
import com.google.gson.Gson;
import com.kredivo.test.activity.BaseActivity;

import butterknife.ButterKnife;

public abstract class BaseFragment extends APIBaseFragment implements RecyclerCallback {

    public static final String TAG = BaseFragment.class.getName();

    protected Gson gson = new Gson();


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(contentXmlLayout(), null);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

    }

    @Override
    public String getTAG() {
        return TAG;
    }

    @Override
    public Object getObject() {
        return null;
    }

    public <ACT extends BaseActivity> ACT act(Class<ACT> act){
        return act.cast(getActivity());
    }
    public <ACT extends BaseActivity> ACT act(){
        return (ACT)getActivity();
    }


    public abstract void onShowing();

}
