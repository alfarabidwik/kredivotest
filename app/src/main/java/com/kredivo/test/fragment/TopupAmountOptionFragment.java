package com.kredivo.test.fragment;


import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.alfarabi.alfalibs.adapters.recyclerview.BasicActRecyclerAdapter;
import com.alfarabi.alfalibs.adapters.recyclerview.BasicFragRecyclerAdapter;
import com.alfarabi.alfalibs.tools.SimpleTextWatcher;
import com.alfarabi.alfalibs.tools.WLog;
import com.alfarabi.alfalibs.views.AlfaRecyclerView;
import com.github.rubensousa.gravitysnaphelper.GravitySnapHelper;
import com.kredivo.test.R;
import com.kredivo.test.dummy.Dummy;
import com.kredivo.test.holder.BottomPromoHolder;
import com.kredivo.test.holder.TopupAmountHolder;
import com.kredivo.test.model.Promo;
import com.kredivo.test.model.Provider;
import com.kredivo.test.model.TopupAmount;
import com.kredivo.test.utils.EqualSpacingItemDecoration;
import com.tbruyelle.rxpermissions2.RxPermissions;
import com.wafflecopter.multicontactpicker.ContactResult;
import com.wafflecopter.multicontactpicker.LimitColumn;
import com.wafflecopter.multicontactpicker.MultiContactPicker;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import lombok.Getter;
import lombok.Setter;

public class TopupAmountOptionFragment extends BaseFragment {

    public static final String TAG = TopupAmountOptionFragment.class.getName();
    public static final int CONTACT_REQUEST = 1;

    @BindView(R.id.recyclerview) AlfaRecyclerView recyclerView ;
    @BindView(R.id.contact_iv) ImageView contactImageView ;
    @BindView(R.id.bottom_recyclerview) AlfaRecyclerView bottomRecyclerView ;
    @BindView(R.id.imageview) ImageView providerLogoImageView ;

    @Getter@Setter @BindView(R.id.cell_number_et) EditText cellNumberEditText ;

    @Getter@Setter private Provider provider ;
    @Getter@Setter int topupType ;

    BasicFragRecyclerAdapter<TopupAmount, TopupAmountOptionFragment, TopupAmountHolder> adapter ;

    BasicFragRecyclerAdapter<Promo, TopupAmountOptionFragment, BottomPromoHolder> bottomAdapter ;


    public static TopupAmountOptionFragment instance(int topupType){
        TopupAmountOptionFragment topupAmountOptionFragment = new TopupAmountOptionFragment();
        topupAmountOptionFragment.setTopupType(topupType);
        return topupAmountOptionFragment ;
    }


    @Override
    public void onShowing() { }

    @Override
    public int contentXmlLayout() {
        return R.layout.fragment_topup_amount_option;
    }

    @Override
    public void onStop() {
        super.onStop();
        cellNumberEditText.setText("");
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        adapter = new BasicFragRecyclerAdapter<>(this, TopupAmountHolder.class, new ArrayList<>());
        adapter.initRecyclerView(recyclerView, new LinearLayoutManager(getActivity()));

        recyclerView.addItemDecoration(new EqualSpacingItemDecoration(12, EqualSpacingItemDecoration.VERTICAL));
        TextView instructionTextView = recyclerView.getEmptyView().findViewById(R.id.instruction_tv);

        cellNumberEditText.addTextChangedListener(new SimpleTextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                boolean valid = true ;
                if(s.length()>3){
                    valid = false ;
                    String input = s.toString();
                    if(input.startsWith("0") || input.startsWith("62") || input.startsWith("+62")){
                        if(input.startsWith("0")){
                            input = input.replaceFirst("0", "");
                            valid = true ;
                        }else if(input.startsWith("62")){
                            input = input.replaceFirst("62", "");
                            valid = true ;
                        }else if(input.startsWith("+62")){
                            input = input.replaceFirst("\\+62", "");
                            valid = true ;
                        }
                        if(input.length()>2){
                            input = input.substring(0,3);
                            provider = Dummy.provider(act(), input, topupType);
                            if(provider!=null){
                                adapter.setObjects(provider.getTopupAmounts());
                                int imageId = act().getResources().getIdentifier(provider.getCompanyLogo(), "drawable", act().getPackageName());
                                providerLogoImageView.setImageResource(imageId);
                                return;
                            }else{
                                provider = null ;
                                instructionTextView.setTextColor(Color.RED);
                                instructionTextView.setText(R.string.invalid_prefix);
                                adapter.setObjects(new ArrayList<>());
                                providerLogoImageView.setImageResource(R.drawable.ic_smartphone);
                                return;
                            }
                        }
                    }
                }
                provider = null ;
                adapter.setObjects(new ArrayList<>());
                providerLogoImageView.setImageResource(R.drawable.ic_smartphone);
                if(valid){
                    instructionTextView.setTextColor(Color.BLACK);
                    instructionTextView.setText(R.string.enter_a_phone_number);
                }else{
                    instructionTextView.setTextColor(Color.RED);
                    instructionTextView.setText(R.string.invalid_prefix);
                }
                return;
            }
        });

        bottomAdapter = new BasicFragRecyclerAdapter<>(this, BottomPromoHolder.class, Dummy.promos(act(), topupType));
        bottomAdapter.initRecyclerView(bottomRecyclerView, new LinearLayoutManager(act(), LinearLayout.HORIZONTAL, false){
            @Override
            public boolean checkLayoutParams(RecyclerView.LayoutParams lp) {
                lp.width = getWidth() - (getWidth()/5);
                return true;
            }
        });
        bottomRecyclerView.addItemDecoration(new EqualSpacingItemDecoration(16, EqualSpacingItemDecoration.HORIZONTAL));
        new GravitySnapHelper(Gravity.START).attachToRecyclerView(bottomRecyclerView);

    }

    @OnClick(R.id.contact_iv)
    void onClick(){
        new RxPermissions(act()).request(Manifest.permission.READ_CONTACTS).subscribe(aBoolean -> {
            if(aBoolean){
                new MultiContactPicker.Builder(this).hideScrollbar(false).showTrack(true).searchIconColor(Color.WHITE)
                        .setChoiceMode(MultiContactPicker.CHOICE_MODE_SINGLE)
                        .handleColor(ContextCompat.getColor(act(), R.color.colorPrimary))
                        .bubbleColor(ContextCompat.getColor(act(), R.color.colorPrimary)).bubbleTextColor(Color.WHITE)
                        .limitToColumn(LimitColumn.NONE).setActivityAnimations(android.R.anim.fade_in, android.R.anim.fade_out, android.R.anim.fade_in, android.R.anim.fade_out)
                        .setTitleText("Select Contacts").setLoadingType(MultiContactPicker.LOAD_ASYNC)
                        .showPickerForResult(CONTACT_REQUEST);
            }
        }).dispose();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == CONTACT_REQUEST){
            if(resultCode == Activity.RESULT_OK) {
                List<ContactResult> results = MultiContactPicker.obtainResult(data);
                ContactResult contactResult = results.get(0);
                if(contactResult.getPhoneNumbers()!=null && contactResult.getPhoneNumbers().size()>0){
                    String number = contactResult.getPhoneNumbers().get(0).getNumber();
                    number = number.replaceAll("[^\\d]", "" );
                    cellNumberEditText.setText(number);
                }else{
                    act().showSnackbar(getString(R.string.no_phone_number_exist), v -> {});
                }
            }
        }
    }
}
