package com.kredivo.test.dummy;

import android.app.Activity;
import android.content.res.AssetManager;

import com.alfarabi.alfalibs.tools.WLog;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.kredivo.test.model.Promo;
import com.kredivo.test.model.Provider;
import com.kredivo.test.model.TopupAmount;
import com.kredivo.test.utils.FileUtils;
import com.kredivo.test.utils.AppStringUtils;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class Dummy {

    public static final int PHONE_BALANCE = 1;
    public static final int QUOTA_BALANCE = 2;

    public static final String TAG = Dummy.class.getName();


    public static Provider provider(Activity activity, String input, int topupType){
        if(input.length()<3){
            return null ;
        }
        WLog.d(TAG, input);
        Gson gson = new Gson();
        String sGson = "";
        try {
            String filename = "";
            if(topupType==PHONE_BALANCE){
                filename = "balance_provider.json";
            }else {
                filename = "internet_provider.json";
            }
            InputStream in = activity.getAssets().open(filename, AssetManager.ACCESS_BUFFER);
            sGson = FileUtils.streamToString(in);
            in.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Type type = new TypeToken<List<Provider>>(){}.getType();
        List<Provider> providers = gson.fromJson(sGson, type);
        Provider returnProvider = null ;
        loop1 :
        for (Provider provider : providers) {
            String[] prefixes = provider.getPrefix().split(" ");
            loop2 :
            for (int i = 0; i < prefixes.length; i++) {
                String prefix = prefixes[i];
                if (prefix.equals(input)){
                    returnProvider = provider ;
                    break loop2;
                }
            }
            if(returnProvider!=null){
                break loop1;
            }
        }
        return returnProvider ;

    }

    public static List<Promo> promos(Activity activity, int type){
        if(type==PHONE_BALANCE){
            return balancePromos(activity);
        }else{
            return internetPromos(activity);
        }
    }
    public static List<Promo> balancePromos(Activity activity){
        String sHTML = "";
        try {
            String filename = "promo_description.html";
            InputStream in = activity.getAssets().open(filename, AssetManager.ACCESS_BUFFER);
            sHTML = FileUtils.streamToString(in);
            in.close();
        } catch (IOException e) {
            e.printStackTrace();
        }


        List<Promo> promos = new ArrayList<>();
        Promo promo = new Promo();
        promo.setId(1l);
        promo.setTitle("Discount 20 % at Kedai Hape Original, Mall Kota Casablanca");
        promo.setProvider("Bukalapak");
        promo.setPercentCashback("8 %");
        promo.setUrl("https://asset-a.grid.id/crop/0x0:0x0/700x465/photo/2018/10/25/4022691018.png");
        promo.setTermAndCondition(sHTML);

        Calendar startCalendar = Calendar.getInstance();
        startCalendar.set(Calendar.DAY_OF_MONTH, startCalendar.get(Calendar.DAY_OF_MONTH)-20);

        Calendar endCalendar = Calendar.getInstance();
        endCalendar.set(Calendar.DAY_OF_MONTH, startCalendar.get(Calendar.DAY_OF_MONTH)+20);
        promo.setStartDate(startCalendar.getTime());
        promo.setEndDate(endCalendar.getTime());
        promo.setVoucherCode(AppStringUtils.randomCode(8));
        promo.build();

        promos.add(promo);

        promo = new Promo();
        promo.setId(2l);
        promo.setTitle("Discount 30 % at Pulsa Rakyat Ciputat, Blok M Mall");
        promo.setProvider("Tokopedia");
        promo.setPercentCashback("10 %");
        promo.setUrl("https://cdn2.tstatic.net/lampung/foto/bank/images/promo-paket-internet-xl-maret-2019-beli-pakai-aplikasi-grab-diskon-sampai-11-persen.jpg");
        promo.setTermAndCondition(sHTML);

        startCalendar = Calendar.getInstance();
        startCalendar.set(Calendar.DAY_OF_MONTH, startCalendar.get(Calendar.DAY_OF_MONTH)-10);

        endCalendar = Calendar.getInstance();
        endCalendar.set(Calendar.DAY_OF_MONTH, startCalendar.get(Calendar.DAY_OF_MONTH)+15);
        promo.setStartDate(startCalendar.getTime());
        promo.setEndDate(endCalendar.getTime());
        promo.setVoucherCode(AppStringUtils.randomCode(8));
        promo.build();
        promos.add(promo);

        promo = new Promo();
        promo.setId(3l);
        promo.setTitle("Discount 50 % at Toko Komputer Digi Daga, Blok M Mall");
        promo.setProvider("Shopee");
        promo.setPercentCashback("15 %");
        promo.setUrl("https://www.gadgetgan.com/wp-content/uploads/2015/12/Promo-Kuota-XL-Super-Deal-Di-Desember-2015.jpg");
        promo.setTermAndCondition(sHTML);

        startCalendar = Calendar.getInstance();
        startCalendar.set(Calendar.DAY_OF_MONTH, startCalendar.get(Calendar.DAY_OF_MONTH)-15);

        endCalendar = Calendar.getInstance();
        endCalendar.set(Calendar.DAY_OF_MONTH, startCalendar.get(Calendar.DAY_OF_MONTH)+30);
        promo.setStartDate(startCalendar.getTime());
        promo.setEndDate(endCalendar.getTime());
        promo.setVoucherCode(AppStringUtils.randomCode(8));
        promo.build();
        promos.add(promo);


        promo = new Promo();
        promo.setId(4l);
        promo.setTitle("Discount 30 % at Cari Pulsa, Sunter Mall");
        promo.setProvider("Lazada");
        promo.setPercentCashback("25 %");
        promo.setUrl("https://promo.sepulsa.com/fbo2017/img/171113_landing(spesial)_harbolnasxl.png");
        promo.setTermAndCondition(sHTML);

        startCalendar = Calendar.getInstance();
        startCalendar.set(Calendar.DAY_OF_MONTH, startCalendar.get(Calendar.DAY_OF_MONTH)-7);

        endCalendar = Calendar.getInstance();
        endCalendar.set(Calendar.DAY_OF_MONTH, startCalendar.get(Calendar.DAY_OF_MONTH)+60);
        promo.setStartDate(startCalendar.getTime());
        promo.setEndDate(endCalendar.getTime());
        promo.setVoucherCode(AppStringUtils.randomCode(8));
        promo.build();
        promos.add(promo);

        return promos ;
    }

    public static List<Promo> internetPromos(Activity activity){
        String sHTML = "";
        try {
            String filename = "promo_description.html";
            InputStream in = activity.getAssets().open(filename, AssetManager.ACCESS_BUFFER);
            sHTML = FileUtils.streamToString(in);
            in.close();
        } catch (IOException e) {
            e.printStackTrace();
        }


        List<Promo> promos = new ArrayList<>();
        Promo promo = new Promo();
        promo.setId(1l);
        promo.setTitle("Grab dan Telkomsel Membuat Promo Paket Quota Internet Bagi Driver Grab");
        promo.setProvider("Grab");
        promo.setPercentCashback("10 %");
        promo.setUrl("https://3.bp.blogspot.com/-zkQr-mlub4g/Wrd5727Dp9I/AAAAAAAASp8/qiTU3H5UUwgI6mnEvE5mM1T_2iiOW5CoQCLcBGAs/s1600/PROMO%2BPaket%2BInternet%2BTelkomsel%2BMurah%2B25GB%2BTanpa%2BPembagian%2BWaktu.jpg");
        promo.setTermAndCondition(sHTML);

        Calendar startCalendar = Calendar.getInstance();
        startCalendar.set(Calendar.DAY_OF_MONTH, startCalendar.get(Calendar.DAY_OF_MONTH)-20);

        Calendar endCalendar = Calendar.getInstance();
        endCalendar.set(Calendar.DAY_OF_MONTH, startCalendar.get(Calendar.DAY_OF_MONTH)+20);
        promo.setStartDate(startCalendar.getTime());
        promo.setEndDate(endCalendar.getTime());
        promo.setVoucherCode(AppStringUtils.randomCode(8));
        promo.build();

        promos.add(promo);

        promo = new Promo();
        promo.setId(2l);
        promo.setTitle("Promo XL Xmartplan Terbaru - Harga Murah");
        promo.setProvider("Blibli");
        promo.setPercentCashback("8 %");
        promo.setUrl("https://i0.wp.com/www.payfazz.com/wp-content/uploads/2019/01/BLOG_PAYFAZZ_PROMOHARGACORET-1.jpg?fit=1200%2C628&ssl=1");
        promo.setTermAndCondition(sHTML);

        startCalendar = Calendar.getInstance();
        startCalendar.set(Calendar.DAY_OF_MONTH, startCalendar.get(Calendar.DAY_OF_MONTH)-10);

        endCalendar = Calendar.getInstance();
        endCalendar.set(Calendar.DAY_OF_MONTH, startCalendar.get(Calendar.DAY_OF_MONTH)+15);
        promo.setStartDate(startCalendar.getTime());
        promo.setEndDate(endCalendar.getTime());
        promo.setVoucherCode(AppStringUtils.randomCode(8));
        promo.build();
        promos.add(promo);

        promo = new Promo();
        promo.setId(3l);
        promo.setTitle("Buy reload 150k at Bank muamalat, Get bonus extra quota 5gb + speed booster");
        promo.setProvider("Muamalat");
        promo.setPercentCashback("15 %");
        promo.setUrl("https://assets.indosatooredoo.com/Assets/Upload/01%20Personal/Promo/Bank/Muamalat/WEB_MUAMALAT_ENGLISH_1024-x-512.jpg");
        promo.setTermAndCondition(sHTML);

        startCalendar = Calendar.getInstance();
        startCalendar.set(Calendar.DAY_OF_MONTH, startCalendar.get(Calendar.DAY_OF_MONTH)-15);

        endCalendar = Calendar.getInstance();
        endCalendar.set(Calendar.DAY_OF_MONTH, startCalendar.get(Calendar.DAY_OF_MONTH)+30);
        promo.setStartDate(startCalendar.getTime());
        promo.setEndDate(endCalendar.getTime());
        promo.setVoucherCode(AppStringUtils.randomCode(8));
        promo.build();
        promos.add(promo);


        promo = new Promo();
        promo.setId(4l);
        promo.setTitle("MANTUL (Mantap Betul), GRATIS Biaya Akses Internet 4 bulan untuk pelanggan baru yang berlangganan UBIQU Paket FIT 4");
        promo.setProvider("UBIQU");
        promo.setPercentCashback("25 %");
        promo.setUrl("https://ubiqu.id/wp-content/uploads/2018/12/FITUL_2.png");
        promo.setTermAndCondition(sHTML);

        startCalendar = Calendar.getInstance();
        startCalendar.set(Calendar.DAY_OF_MONTH, startCalendar.get(Calendar.DAY_OF_MONTH)-7);

        endCalendar = Calendar.getInstance();
        endCalendar.set(Calendar.DAY_OF_MONTH, startCalendar.get(Calendar.DAY_OF_MONTH)+60);
        promo.setStartDate(startCalendar.getTime());
        promo.setEndDate(endCalendar.getTime());
        promo.setVoucherCode(AppStringUtils.randomCode(8));
        promo.build();
        promos.add(promo);

        return promos ;
    }


    public static final List<TopupAmount> phoneBalances(){
        List<TopupAmount> topupAmounts = new ArrayList<>();

        TopupAmount topupAmount = new TopupAmount();
        topupAmount.setId(1l);
        topupAmount.setAmount(new BigDecimal(25000));
        topupAmounts.add(topupAmount);

        topupAmount = new TopupAmount();
        topupAmount.setId(2l);
        topupAmount.setAmount(new BigDecimal(50000));
        topupAmounts.add(topupAmount);

        topupAmount = new TopupAmount();
        topupAmount.setId(3l);
        topupAmount.setAmount(new BigDecimal(100000));
        topupAmounts.add(topupAmount);

        topupAmount = new TopupAmount();
        topupAmount.setId(4l);
        topupAmount.setAmount(new BigDecimal(150000));
        topupAmounts.add(topupAmount);

        topupAmount = new TopupAmount();
        topupAmount.setId(5l);
        topupAmount.setAmount(new BigDecimal(195000));
        topupAmounts.add(topupAmount);

        return topupAmounts ;
    }


    public static final List<TopupAmount> internetBalances(){
        List<TopupAmount> topupAmounts = new ArrayList<>();

        TopupAmount topupAmount = new TopupAmount();
        topupAmount.setId(1l);
        topupAmount.setAmount(new BigDecimal(25000));
        topupAmounts.add(topupAmount);

        topupAmount = new TopupAmount();
        topupAmount.setId(2l);
        topupAmount.setAmount(new BigDecimal(50000));
        topupAmounts.add(topupAmount);

        topupAmount = new TopupAmount();
        topupAmount.setId(3l);
        topupAmount.setAmount(new BigDecimal(100000));
        topupAmounts.add(topupAmount);

        topupAmount = new TopupAmount();
        topupAmount.setId(4l);
        topupAmount.setAmount(new BigDecimal(150000));
        topupAmounts.add(topupAmount);

        topupAmount = new TopupAmount();
        topupAmount.setId(5l);
        topupAmount.setAmount(new BigDecimal(195000));
        topupAmounts.add(topupAmount);

        return topupAmounts ;
    }



}
