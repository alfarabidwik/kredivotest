package com.kredivo.test.model;

import org.parceler.Parcel;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@Parcel
@EqualsAndHashCode(callSuper = false)
public class Provider extends BaseModel{

    String name ;
    String companyLogo ;
    String prefix ;
    List<TopupAmount> topupAmounts = new ArrayList<>();
}
