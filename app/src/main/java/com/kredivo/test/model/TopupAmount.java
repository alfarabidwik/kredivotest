package com.kredivo.test.model;

import org.parceler.Parcel;

import java.math.BigDecimal;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@Parcel
@EqualsAndHashCode(callSuper = false)
public class TopupAmount extends BaseModel {

    BigDecimal amount = BigDecimal.ZERO;
    String name ;
}
