package com.kredivo.test.model;

import org.parceler.Parcel;

import lombok.Data;

@Data
@Parcel
public class BaseModel {

    Long id ;

}
