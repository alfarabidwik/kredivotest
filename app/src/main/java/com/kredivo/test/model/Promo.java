package com.kredivo.test.model;

import org.apache.commons.lang.StringUtils;
import org.parceler.Parcel;

import java.util.Date;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@Parcel
@EqualsAndHashCode(callSuper = false)
public class Promo extends BaseModel{
    String url ;
    String title ;
    Date startDate ;
    Date endDate ;
    String voucherCode ;
    String termAndCondition ;
    String provider ;
    String percentCashback ;

    public void build(){
        String termAndCondition1 = termAndCondition ;
        termAndCondition1 = StringUtils.replace(termAndCondition1, "#PROVIDER#", getProvider());
        termAndCondition1 = StringUtils.replace(termAndCondition1, "#VOUCHER_CODE#", getVoucherCode());
        termAndCondition1 = StringUtils.replace(termAndCondition1, "#PERCENT_CASHBACK#", getPercentCashback());
        setTermAndCondition(termAndCondition1);
    }

}
