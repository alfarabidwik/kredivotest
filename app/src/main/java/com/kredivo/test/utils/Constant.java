package com.kredivo.test.utils;

public class Constant {

    public static final String INDONESIA_COUNTRY_ID = "ID";
    public static final String INDONESIA_LANGUAGE_ID = "in";
    public static final String LANGUAGE = "language";


    public static final String REPORT_RECEIVER_EMAIL = "alfarabidwik@gmail.com";
    public static final String REPORT_SUBJECT = Constant.class.getPackage().getName();



    public static int RESULT_FINISH = 1111;
    public static int REQUEST_TOPUP = 5555;


}
