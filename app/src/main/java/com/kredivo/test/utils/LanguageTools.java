package com.kredivo.test.utils;

import android.app.Activity;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.preference.PreferenceManager;

import com.alfarabi.alfalibs.tools.WLog;

import java.util.Locale;

/**
 * Created by Alfarabi on 10/18/17.
 */

public class LanguageTools {
    public static final String TAG = LanguageTools.class.getName();

    public static void initLanguage(Activity context){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        String language = prefs.getString(Constant.LANGUAGE, Constant.INDONESIA_LANGUAGE_ID);
//        String language  = "in"; // your language
        Locale locale = new Locale(language);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        context.getResources().updateConfiguration(config, context.getResources().getDisplayMetrics());
    }


    public static final String getLanguage(String countryId){
        String lang = "";
        for (Locale locale : Locale.getAvailableLocales()){
            if(locale.getCountry().equalsIgnoreCase(countryId)){
                lang = locale.getLanguage();
                break;
            }
        }
        WLog.d(TAG, "LANGUAGE ID ### "+lang);
        if(lang.isEmpty()){
            lang = Constant.INDONESIA_LANGUAGE_ID;
        }
        return lang;
    }
}
