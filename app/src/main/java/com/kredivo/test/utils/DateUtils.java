package com.kredivo.test.utils;

import org.apache.commons.lang.time.DateFormatUtils;

import java.util.Calendar;
import java.util.Date;

public class DateUtils {


    public static final String validDate(Date startDate, Date endDate){
        if(startDate==null || endDate==null){
            return "";
        }

        Calendar startCalendar = Calendar.getInstance();
        startCalendar.setTime(startDate);

        Calendar endCalendar = Calendar.getInstance();
        endCalendar.setTime(endDate);

        boolean sameYear = false ;
        boolean sameMonth = false ;

        if(startCalendar.get(Calendar.YEAR)==endCalendar.get(Calendar.YEAR)){
            sameYear = true ;
        }

        if(startCalendar.get(Calendar.MONTH)==endCalendar.get(Calendar.MONTH)){
            sameMonth = true ;
        }

        if(sameYear&&sameMonth){
            return DateFormatUtils.format(startDate, startCalendar.get(Calendar.DAY_OF_MONTH)<10?"d":"dd")+" - "+DateFormatUtils.format(endDate, endCalendar.get(Calendar.DAY_OF_MONTH)<10?"d":"dd")+" "+DateFormatUtils.format(endDate, "MMMM yyyy");
        }
        if(sameYear){
            return DateFormatUtils.format(startDate, startCalendar.get(Calendar.DAY_OF_MONTH)<10?"d MMMM":"dd MMMM")+" - "+DateFormatUtils.format(endDate, endCalendar.get(Calendar.DAY_OF_MONTH)<10?"d MMMM":"dd MMMM")+" "+DateFormatUtils.format(endDate, "yyyy");
        }
        return DateFormatUtils.format(startDate, startCalendar.get(Calendar.DAY_OF_MONTH)<10?"d MMMM yyyy":"dd MMMM yyyy")+" - "+DateFormatUtils.format(endDate, endCalendar.get(Calendar.DAY_OF_MONTH)<10?"d MMMM yyyy":"dd MMMM yyyy");


    }

}
