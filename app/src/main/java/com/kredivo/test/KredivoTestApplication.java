package com.kredivo.test;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;

import com.alfarabi.alfalibs.AlfaLibsApplication;
import com.alfarabi.alfalibs.tools.UISimulation;
import com.kredivo.test.utils.Constant;

import net.soroushjavdan.customfontwidgets.FontUtils;

public class KredivoTestApplication extends AlfaLibsApplication {

    @Override
    public void onCreate() {
        super.onCreate();
        Thread.setDefaultUncaughtExceptionHandler((thread, throwable) -> {
            throwable.printStackTrace();
        });

        this.DEBUG = false ;
        UISimulation.mode = UISimulation.DEV_REALDATA;
        FontUtils.createFonts(this, "fonts");
        FontUtils.setDefaultFont(this, "fonts", "Montserrat-ExtraLight");
        //        Kitchen.preparation(Constant.BASE_API, 30, 60, 60, true);
//        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
    }

    @Override
    protected boolean getBuildConfigDebug() {
        return DEBUG;
    }

    @Override
    protected String getBuildConfigVersionName() {
        PackageInfo pInfo = null;
        try {
            pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            String version = pInfo.versionName;
            return version;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return "0";
    }

    @Override
    public String emailReportApps() {
        return Constant.REPORT_RECEIVER_EMAIL;
    }

    @Override
    public String subjectLineReportApps() {
        return Constant.REPORT_SUBJECT;
    }

}
