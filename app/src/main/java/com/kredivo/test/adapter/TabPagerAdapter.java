package com.kredivo.test.adapter;

import android.support.v4.app.FragmentManager;

import com.alfarabi.alfalibs.adapters.viewpager.BaseTabPagerAdapter;
import com.kredivo.test.activity.BaseActivity;
import com.kredivo.test.fragment.BaseFragment;

import java.util.List;

import lombok.Getter;

public class TabPagerAdapter<A extends BaseActivity> extends BaseTabPagerAdapter<BaseFragment> {

    @Getter String[] titles ;
    @Getter BaseFragment[] fragments ;


    public TabPagerAdapter(FragmentManager fm, List<String> titleList, List<BaseFragment> fragmentList) {
        super(fm);
        this.titles = new String[titleList.size()];
        this.fragments = new BaseFragment[titleList.size()];
        for (int i = 0; i < titleList.size(); i++) {
            this.titles[i] = titleList.get(i);
        }
        for (int i = 0; i < fragmentList.size(); i++) {
            this.fragments[i] = fragmentList.get(i);
        }
    }

    @Override
    public String[] titles() {
        return this.titles;
    }

    @Override
    public BaseFragment[] fragmentClasses() {
        return this.fragments;
    }
}
